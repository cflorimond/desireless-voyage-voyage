﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.Models
{
    public interface ICatalogRepository : IDisposable
    {
        IEnumerable<CatalogTrip> GetTrip(int pageSize, int pageNum);
        CatalogTrip GetTripById(int idTrip);
    }
}